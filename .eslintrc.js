module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'prettier',
  ],
  overrides: [
    {
      files: ['*.svelte'],
      processor: 'svelte3/svelte3',
    },
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: ['svelte3', '@typescript-eslint', 'prettier'],
  rules: {
    'no-plusplus': 'off',
    // 'prettier/prettier': 'error',
  },
  settings: {
    'svelte3/typescript': require('typescript'),
  },
  ignorePatterns: ['node_modules', 'public/build'],
};
