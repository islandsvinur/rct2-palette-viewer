import { Color } from './color';
import { Palette } from './palette';

function toInt32(data: Uint8Array, number: number): number {
  /* eslint-disable no-bitwise */
  return (
    (data[number] |
      (data[number + 1] << 8) |
      (data[number + 2] << 16) |
      (data[number + 3] << 24)) >>>
    0
  );
  /* eslint-enable no-bitwise */
}

function concat(...arrays: Uint8Array[]): Uint8Array {
  const result = new Uint8Array(arrays.reduce((len, a) => len + a.length, 0));
  let j = 0;
  for (let i = 0; i < arrays.length; i++) {
    result.set(arrays[i], j);
    j += arrays[i].length;
  }
  return result;
}

function getColors(data: Uint8Array): Color[] {
  const colors: Color[] = new Array(data.length / 3);
  for (let i = 0; i < colors.length; i++) {
    colors[i] = new Color(data[i * 3 + 2], data[i * 3 + 1], data[i * 3]);
  }

  return colors;
}

function loadDatPaletteFromBuffer(buffer: ArrayBuffer): Color[] {
  const data = new Uint8Array(buffer);

  if (data[16] !== 0x01) {
    // eslint-disable-next-line no-console
    console.log(
      `This DAT file uses the encoding '${data[16]}' which is not supported.`
    );
  }

  const block = data.slice(21, toInt32(data, 17));

  const decodedBytesList: Uint8Array[] = [];
  for (let i = 0; i < block.length; ) {
    if (block[i] > 0x7f) {
      const value = block[i + 1];
      const count = 257 - block[i];
      const array = new Uint8Array(count);
      array.fill(value);
      decodedBytesList.push(array);
      i += 2;
    } else {
      const values = block.slice(i + 1, i + 1 + block[i] + 1);
      decodedBytesList.push(values);
      i += block[i] + 2;
    }
  }
  const decodedBytes = concat(...decodedBytesList);

  let s = 0;
  let str = [];
  const strings = [];
  do {
    const decodedByte = decodedBytes[s];
    if (decodedByte > 0) {
      str.push(decodedByte);
    } else {
      strings.push(str);
      str = [];
      s++;
    }
    s++;
  } while (decodedBytes[s] !== 0xff && decodedBytes[s - 1] === 0x00);

  const endOfStringTable =
    decodedBytes.findIndex(
      (_, i, arr) => i > 0 && arr[i] === 0xff && arr[i - 1] === 0x00
    ) + 1;
  const imageDirectoryLength = toInt32(decodedBytes, endOfStringTable);

  const start = endOfStringTable + 8 + imageDirectoryLength * 16;
  const colorData = decodedBytes.slice(start);

  return getColors(colorData);
}

export async function loadDatPalette(file: File): Promise<Palette> {
  return new Palette(loadDatPaletteFromBuffer(await file.arrayBuffer()));
}
