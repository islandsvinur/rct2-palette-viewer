import type { Color } from './color';

export class Palette {
  constructor(public colors: Color[]) {}
}
