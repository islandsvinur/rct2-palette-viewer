import tinycolor from 'tinycolor2';

export class Color {
  private color: tinycolor;

  constructor(private r: number, private g: number, private b: number) {
    this.color = tinycolor(`rgb(${r}, ${g}, ${b})`);
  }

  toHexRgb() {
    const R = this.r.toString(16);
    const G = this.g.toString(16);
    const B = this.b.toString(16);
    return `#${R.length === 1 ? `0${R}` : R}${G.length === 1 ? `0${G}` : G}${
      B.length === 1 ? `0${B}` : B
    }`;
  }
}
